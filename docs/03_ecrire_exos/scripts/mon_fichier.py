# --- PYODIDE:code --- #

# Ecrire en compréhension la liste ma_liste = [0, 1, 2, 3, 4, 5]
ma_liste = ...

# --- PYODIDE:corr --- #

ma_liste = [i for i in range(5)]


# --- PYODIDE:tests --- #

assert ma_liste = [0, 1, 2, 3, 4, 5]

# --- PYODIDE:secrets --- #

assert ma_liste = [0, 1, 2, 3, 4, 5]




