---
author: Éric AIMÉ
title: 🏡 Accueil
---


# ![magic](images/magic-wand_1fa84.png){ width=5% } Tutoriels et aides pour l'enseignant qui crée son site avec Python intégré

!!! info "Ce tutoriel"

    Ce site est largement inspiré du travail réalisé par F. JUNIER au Lycée du Parc
    Grand merci également aux auteurs des tutos, notamment Mireille Coilhac et Bertrand Chartier.



!!! danger "Attention : Mise à jour à effectuer si vous avez créé votre site avant le 26/08/2024"

    Le passage à Pyodide MkDocs Theme v.2.2.0 se fera automatiquement au prochain commit. Pour ne pas mettre le pipeline en échec, il faut dans le fichier `mkdocs.yml` vers la ligne 130 : 

    Remplacer  

    ```yaml title="À modifier"
      - material/search
      - material/tags:
          tags_file: tags.md
    ```

    par : 

    ```yaml title="À mettre à la place"
      - search
      - tags:
          tags_file: tags.md
    ```

!!! info "Un parcours pour construire votre site pas à pas"

    [Parcours pas à pas](./parcours/pas_a_pas.md)

!!! danger "Attention : Migration à programmer de la forge AEIF vers la forge Education Nationale"

    La migration devra être réalisée avant le 31 juin 2024. [réaliser la migration et la redirection](migration/migrer.md){ .md-button target="_blank" rel="noopener" }   



!!! warning "Les sites compatibles avec ce tutoriel"

    Votre site n'est pas forcément compatible avec ce tutoriel

    ??? note "😥 Cliquer pour voir les sites non compatibles"

        Le contenu de ce tutoriel est inadapté si votre site a été construit à partir de l'ancienne version pyodide 
        et n'a pas été mis à jour.
        
        Vous pouvez : 

        * Faire la mise à jour : 
        
        [MAJ](maj/mise_a_jour_theme_pyodide.md){ .md-button target="_blank" rel="noopener" }

        * Ne pas faire la mise à jour, et vous reporter à l'ancien tutoriel. Cette option est déconseillée car 
        la mise à jour amène beaucoup d'améliorations.
        
        [Ancien tutoriel](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/){ .md-button target="_blank" rel="noopener" }


    ??? note "😊 Cliquer pour voir les sites compatibles"

        Le contenu de ce tutoriel est adapté si 
        
        * votre site a été construit à partir de l'ancienne version pyodide 
        et a  été mis à jour avec Pyodide Mkdocs Theme (par exemple ceci visible en bas du site à droite) :  

        ![version du thème possible après mise à jour](maj/images/version_theme.png){ width=25% }

        * ou s'il a été construit en utilisant le site modèle donné ci-dessous


## 👉 Le modèle de site avec des exercices interactifs en Python

Ce tutoriel accompagne le modèle suivant à cloner, pour aider à leur prise en main :

[Rendu du site avec pyodide-mkdocs-theme](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/){ .md-button target="_blank" rel="noopener" }
[Dépôt du site avec pyodide-mkdocs-theme à cloner](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review){ .md-button target="_blank" rel="noopener" }  

??? note "Cliquer pour voir le tutoriel pour un site simple sans exercices intéractifs Python"

    [Tutoriel pour un site simple](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/){ .md-button target="_blank" rel="noopener" }  


## 👉 Cloner le modèle de site pour le personnaliser

Vous pouvez faire une bifurcation (on dit aussi un «fork»)  de ces modèles pour réaliser le vôtre.  
Le tutoriel pour réaliser cette bifurcation est sur ce site, dans la rubrique "Comment créer un site à partir d'un autre".

Lien direct : [Faire une «bifurcation» (fork en anglais)](08_tuto_fork/1_fork_projet.md){ .md-button target="_blank" rel="noopener" }


## 👉 Si vous débutez et pour comprendre la structure

Il est conseillé de commencer par : [Avant de démarrer](01_demarrage/1_demarrage.md/){ .md-button target="_blank" rel="noopener" }


!!! abstract "En bref"

    😀 Vous pourrez facilement recopier toutes les syntaxes depuis les différents tutoriels proposés.

    * Le dépôt de ce tuto est ici : [Dépôt du tutoriel](https://forge.apps.education.fr/docs/tutoriels/pyodide-mkdocs-theme-review){ .md-button target="_blank" rel="noopener" }

    * Pour avoir des informations détaillées sur le fonctionnement du thème mkdocs permettant de réaliser les sites, voir la [Documentation détaillée de pyodide-mkdocs-theme par F. Zinelli ](https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme){:target="_blank" }



_Dernière mise à jour le 25/08/2024_
