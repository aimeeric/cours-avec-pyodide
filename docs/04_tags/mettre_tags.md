---
author: Mireille Coilhac
title: Ajouter des tags
---

## I. Dans votre dépôt

!!! info "A vérifier dans le dépôt"

    Si ce n'est pas déjà fait il faut : 

    **1. Créer une page tags.md, puis la mettre dans le dossier `docs`**

    !!! info "Exemple de fichier tags.md"

    	```markdown title="fichier tags.md"
    	# 🏷️ Tags

    	```


    **2. Modifier le fichier mkdocs.yml :**

    Sous `plugins`  ajouter si nécessaire : 

    ```text title="Dans le fichier mkdocs.yml sous plugins recopier :"
    - tags:
        tags_file: tags.md
    ```

## II. Les pages avec tags


Il suffit de compléter l'en-tête de la page `.md`


```markdown title="début du fichier `.md` à recopier"
---
author: Noms d'auteurs
title: Titre de la page
tags:
  - Nom du tag 
---

Suite de ma page ...

```

### Exemple dans "Chapitre 2" dans la page "Une fonction" 


Dans le site modèle par exemple il y a le tag `fonction` : [page Une fonction simple](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/02_chapitre_2/2_fonction/){:target="_blank" }

!!! info "Fichiers utilisés pour la page : Une fonction "

    ````markdown title="2_fonction.md"
    ---
    author: Votre nom
    title: Une fonction
    tags:
      - fonction
    ---

    La fonction `addition` prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux.

    ???+ question "Compléter ci-dessous"
        {% raw %}
        {{ IDE('scripts/addition') }}
        {% endraw %}
    ````

    On peut mettre autant de tags que l'on veut : 

    ````markdown title="ma_page_avec tags.md"
    ---
    author: Votre nom
    title: Ma page d'exercices
    tags:
      - fonction
      - liste/tableaux
      - Difficulté ***
    ---
    ````


## II. Les tags dans le menu de votre site

!!! info "Les tags dans le menu de votre site "

    * 🪄 Aucune autre manipulation  n'est nécessaire. Dans le menu, en cliquant sur Tags ([Tags dans le menu du site modèle](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/tags/){:target="_blank" }) tous les tags apparaîtront de façon automatique.

    * 😊 Si dans vos fichiers `.md` vous en rajoutez ou supprimez des tags, la page de tags du site sera automatiquement mise à jour.



