---
author: Mireille Coilhac
title: En panne 😭
---

## Les FAQ

🌵 Si vous rencontrez des problèmes ...

??? tip "Le rendu n'est pas bon"

    Vous voyez du code html ou markdown s'afficher.

    * Vérifier qu'il n'y a pas d'erreur d'indentation :
    Il n'en faut pas moins, mais pas plus non plus. Bien regarder les modèles proposés dans ce tutoriel.

    * Vérifier qu'une ligne vide nécessaire a bien été mise : avant un tableau, avant une liste à puce ...

    Si vous avez écrit du code entre     
    
    ````markdown 
    ```python
    du code python
    ```
    ````

    vérifier que vous avez bien mis 3 backticks au début et à la fin.

??? tip "Après un commit, cela ne passe pas à la coche verte **réussi** "

    👉 Attendre, puis rafraichir la page dans votre navigateur.

??? tip "Le pipeline est en échec"

    * Pas de panique 😅 ! Il faut commencer par comprendre la cause de l'échec : [Echec du pipeline](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/maj/mise_a_jour_theme_pyodide/#iv-terminer){ .md-button target="_blank" rel="noopener" }

    * Votre site était créé avant le 25/8/2024, et vous avez réalsé un commit après. Une mise à jour est nécessaire : dans le fichier `mkdocs.yml` vers la ligne 130 : 

    Remplacer  

    ```yaml title=""
      - material/search
      - material/tags:
          tags_file: tags.md
    ```

    par : 

    ```yaml title=""
      - search
      - tags:
          tags_file: tags.md
    ```

    !!! info "Erreurs fréquentes qui mettent un pipeline en échec"


        * Un fichier `.pages` incorrect 

        Vérifier qu'il n'y a pas d'erreur dans le fichier `.pages` (suite à des modifications ou suppressions de fichiers par exemple, 
        ou à une faute d'orthographe)

        * Vérifier qu'il n'y a pas d'erreur de syntaxe (indentation par exemple) dans le fichier `mkdocs.yml`

        * Un nom de fichier est invalide : Le thème n’autorise plus les caractères accentués, caractères spéciaux, espaces …   
        Voir : [Mettre à jour les fichiers](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/maj_pyodide_mkdocs/#7-mettre-a-jour-les-fichiers-de-la-documentation){:target="_blank" }

    👉 Si vous avez rectifié une erreur, après un nouveau `commit`, le pipeline devrait passer.


??? tip "L'IDE d'un exercice reste vide, alors que vous avez bien mis un fichier Python"


    * Si c'est votre premier exercice, attendre, rafraichir la page dans le navigateur, rafraichir l'IDE.

    * Vérifier le chemin du fichier devant se trouver dans l'IDE : par exemple, est-il dans un dossier `scripts` ou pas ? 

    * Vérifier les paramètres du projet : 

    👉 Aller sur le dépôt, puis dans Déploiement, puis dans Pages

    ![Aller dans deploiement > pages](../08_tuto_fork/images/trouver_pages.png){ width=20% }

    👉 Il faut  **décocher** "Utiliser un domaine unique" 

    ![Décocher puis enregistrer](../08_tuto_fork/images/decocher_enregistrer.png){ width=60% }

    👉 Ne pas oublier de rafraichir la page du rendu.


??? tip "Le dépôt semble correct, mais le rendu ne correspond pas"

    👉 Essayer dans un autre navigateur. Si cela fonctionne bien, vider la mémoire cache de votre navigateur. Vous trouverez facilement en ligne comment procéder.

??? tip "J'ai créé un répertoire, il a disparu"

    👉 Si vous avez créé un nouveau répertoire sans y mettre de fichier dedans, il a disparu après le commit. Recommencer l'opération en ajoutant un fichier dedans.

??? tip "Une page n'apparaît pas dans le menu"

    👉 Vérifiez que cette page est bien notée dans le fichier `.pages` dans lequel elle devrait être.
    

## Contact en cas de problème :

* Si vous êtes connecté à [Tchap](https://projet.apps.education.fr/apps_externes/tchap){:target="_blank" }  le lien suivant s'ouvrira directement, vous pourrez y trouver de l'aide :  

<a href="https://tchap.gouv.fr/#/room/!BXZZsyWklktciNEDbM:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.intradef.tchap.gouv.fr">
<span aria-label="Avatar" aria-hidden="true" data-type="round" data-color="3" class="_avatar_k41ul_17 mx_BaseAvatar" style="--cpd-avatar-size: 16px;"><img loading="lazy" alt="" src="https://matrix.agent.education.tchap.gouv.fr/_matrix/media/v3/thumbnail/matrix.agent.education.tchap.gouv.fr/de0e2fe63b40dd452178360baa3ff29ba16d8b98?width=16&amp;height=16&amp;method=crop" crossorigin="anonymous" referrerpolicy="no-referrer" class="_image_k41ul_49" data-type="round" width="16px" height="16px"></span><span class="mx_Pill_text">DEV LaForgeEdu</span></a>

* Sinon, se connecter à [Tchap](https://projet.apps.education.fr/apps_externes/tchap){:target="_blank" } et sélectionner DEV LaForgeEdu.

